$(document).ready(function() {
  function update_character_and_font(displaystring) {
    var newstring = $('#string').val().substring(0,1);
    $('#string').val(newstring);
    var typeface = $('input[name=typeface]:checked').val();
    displaystring.attr({text:newstring, 'font-family':typeface});
  }
  var paper = Raphael(document.getElementById('character'), 216, 216);
  paper.setViewBox(0,0,1000,1000);
  paper.setStart();
  var outer_frame = paper.rect(0,0,1000,1000).attr({'stroke-width':5});
  paper.rect(15,15,970,970);
  paper.path('M 0 500 L 1000 500').attr({'stroke-width':2});
  paper.path('M 500 0 L 500 1000').attr({'stroke-width':2});
  paper.path('M 0 0 L 1000 1000').attr({'stroke-dasharray':'. ', 'stroke-width':1});
  paper.path('M 0 1000 L 1000 0').attr({'stroke-dasharray':'. ', 'stroke-width':1});
  var frame = paper.setFinish();
  frame.attr({stroke:'#7a7f53', fill:'transparent'});
  var displaystring = paper.text(500, 500, '永').attr({'font-size':1000, 'font-family':'ACKaisyo', 'fill':'#372f37'});
  update_character_and_font(displaystring);
  $('#update').click(function(e) {
    update_character_and_font(displaystring);
  });
  $('#string').change(function(e) {
    update_character_and_font(displaystring);
  });
  $('input[name=typeface]').change(function(e) {
    update_character_and_font(displaystring);
  });
  // }
});
